/*
 * Authors: T. Heim <timon.heim@cern.ch>,
 * Date: 2014-Oct-16
 */

#include "Fei4ParameterLoop.h"

void Fei4ParameterLoop::writeConfig(json &config){
  config["min"] = min;
  config["max"] = max;
  config["step"] = step;
  config["parameter"] = parName;  
}
void Fei4ParameterLoop::loadConfig(json &config){
  min = config["min"];
  max = config["max"];
  step = config["step"];
  parName = config["parameter"];  
}

